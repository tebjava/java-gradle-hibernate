Program bazodanowy zbudowany o szkielet Gradle. \

Zawiera pliki Hibenrate w postaci xml, przykłady rozwiązań za pomocą adnotacji 
oraz rozwiązanie oparte w pełni na sterowniku MySQL (bez technologii Hibernate). 
Ten projekt służył jako wzór od opisu technologii Hibernate.

Projekt wykonany w technice MVC (Model-View-Controller). 