package Hibernate.Gradle;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import org.hibernate.cfg.Configuration;
import org.hibernate.service.Service;
import org.hibernate.service.ServiceRegistry;

import Hibernate.Gradle.Gra.TYP;
import driver.map.GryDriver;
import driver.map.RoleMap;

public class Main {

	private SessionFactory polaczenieHibernate;
	
	protected void setup() {
//		Configuration cfg = new Configuration();
//		cfg.setProperty("hibernate.connection.username", "test");
//		cfg.setProperty("hibernate.connection.password", "test");
//		cfg.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
//		cfg.setProperty("hibernate.connection.url", "jdbc:mysql://192.168.1.220:3306/esport");
//		cfg.setProperty("hbm2ddl.auto", "update");
//		cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//		cfg.setProperty("show_sql", "true");
//		cfg.addAnnotatedClass(Osoby.class);
//		cfg.addAnnotatedClass(Role.class);

		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
		        .configure() // configures settings from hibernate.cfg.xml
		        .build();
		try {
			//polaczenieHibernate = cfg.buildSessionFactory();
			polaczenieHibernate = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		    
		} catch (Exception ex) {
		    StandardServiceRegistryBuilder.destroy(registry);
		    throw new RuntimeException(ex);
		}
    }
	
	protected void exit() {
    	if (polaczenieHibernate != null) {
    		polaczenieHibernate.close();
    	}
    }
	
	protected SessionFactory getPolaczenie() {
		return polaczenieHibernate;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Projekt Hibenrate Gradle");
		Main hibernate = new Main();
		hibernate.setup();
		
		//System.out.println("Stanowisko o id 2 " + new RoleMap(hibernate.getPolaczenie().openSession()).read(2).getNazwa());
		//System.out.println("Stanowisko id dla Trener " + new RoleMap(hibernate.getPolaczenie().openSession()).read("Trener").getId());
		
		//new RoleMap(hibernate.getPolaczenie().openSession()).save("Techniczny");
		//kod obsługi baz danych
		
		//RoleMap rMapa = new RoleMap(hibernate.getPolaczenie().openSession());
		
		//rMapa.update("Techniczny","Asystent");
		//rMapa.save("Wystawca");
		//rMapa.save("Komentator");
		//rMapa.delete("Sprzątacz");
		
		hibernate.exit();
		
		GryDriver gd=new GryDriver();
		gd.otworzPolaczenie(true);
		gd.zaladuj();
		gd.dodaj("CS:GO");
		gd.dodaj("LoL");
		gd.dodaj("Rocket League");
		for (Gra g: gd.pobierzWszystko()) {
			System.out.println(g.getNazwa());
		}
		int tmpIndex=gd.pobierzIndex("LoL");
		Gra tmp = gd.pobierz(tmpIndex);
		tmp.setNazwa("LoL2");
		tmp.setTryb("multi");
		gd.aktualizuj(tmp.getId());
	
		//gd.dodaj("LoL");
		//gd.zapisz();
		//gd.usun();
		gd.zamknijPolaczenie(true);
	}

}
