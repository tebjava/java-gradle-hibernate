package Hibernate.Gradle;
import javax.persistence.*;

@Entity
@Table(name = "osoby")
public class Osoby {
	@Id
	@Column(name = "id_osoba")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "imie")
	private String name;
	@Column(name = "nazwisko")
	private String surname;
	//tego pola nie trzeba łączyć; nazwa pola w klasie oraz tabeli jest taka sama
	private String nick;
	@Column(name = "data_urodzenia")
	private String birthdate;
	@Column(name = "emial")
	private String email;
	@Column(name = "telefon")
	private String telephone;
	
	public Osoby() {
		
	}
	
	public void setId(long id) {
		this.id=id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setSurname(String surname) {
		this.surname=surname;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setBirthdate(String birthdate) {
		this.birthdate=birthdate;
	}
	
	public String getBirthdate() {
		return birthdate;
	}
	
	public void setNick(String nick) {
		this.nick=nick;
	}
	
	public String getNick() {
		return nick;
	}
	
	public void setEmail(String email) {
		this.email=email;
	}
	
	public String getEmail() {
		return email;
	}
	public void setTelephone(String telephone) {
		this.telephone=telephone;
	}
	
	public String getTelephone() {
		return telephone;
	}
}
