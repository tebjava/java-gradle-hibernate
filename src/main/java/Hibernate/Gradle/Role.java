package Hibernate.Gradle;

//import javax.persistence.*;

//@Entity
//@Table(name = "role")
public class Role {
	//@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Column(name = "id_rola")
	private long id;
	private String nazwa;

	public void setId(long id) {
		this.id=id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setNazwa(String nazwa) {
		this.nazwa=nazwa;
	}
	
	public String getNazwa() {
		return nazwa;
	}
}
