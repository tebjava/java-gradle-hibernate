package Hibernate.Gradle;

public class Gra {
	
	public static enum TYP {
		ID(0),
		NAZWA(1),
		GATUNEK(2),
		TRYB(3);
		
		private int x;
		
		private TYP(int x) {this.x=x;}
		
		public int get() {return x;}
		public static TYP typ(int i) {
			switch (i) {
				case 0: return TYP.ID;
				case 1: return TYP.NAZWA;
				case 2: return TYP.GATUNEK;
				case 3: return TYP.TRYB;
				//default: return null;
			}
			return null;
		}
	}
	
	public final static int ID = 0;
	public final static int NAZWA = 1;
	public final static int GATUNEK = 2;
	public final static int TRYB = 3;
	
	private long id;
	private String nazwa;
	private String gatunek;
	private String tryb;
	
	public Gra() {
		this(null,null,null);
	}
	
	public Gra(String nazwa) {
		this(nazwa,null,null);
	}
	
	public Gra(String nazwa, String gatunek) {
		this(nazwa,gatunek,null);
	}
	
	public Gra(String nazwa, String gatunek, String tryb) {
		this(nazwa,gatunek,tryb,-1);		
	}
	
	public Gra(String nazwa, String gatunek, String tryb, long id) {
		this.nazwa=nazwa;
		this.gatunek=gatunek;
		this.tryb=tryb;
		this.id=id;
	}
	
	public long getId() {
		return id;
	}
	
	public String getNazwa() {
		return nazwa;
	}
	
	public String getGatunek() {
		return gatunek;
	}
	
	public String getTryb() {
		return tryb;
	}
	
	public void setNazwa(String nazwa) {
		this.nazwa=nazwa;
	}
	
	public void setGatunek(String gatunek) {
		this.gatunek=gatunek;
	}
	
	public void setTryb(String tryb) {
		this.tryb=tryb;
	}
	
	public void setGra(Gra gra) {
		this.gatunek=gra.getGatunek();
		this.nazwa=gra.getNazwa();
		this.tryb=gra.getTryb();
		this.id=gra.getId();
	}
	
	public int zawiera(Object o) {
		if (o.getClass().isPrimitive()) {
			if ((long)o == this.id)
				return ID;
		}
		else if (o.getClass() == String.class) {
			if (o.toString().compareTo(this.gatunek)==0)
				return GATUNEK;
			if (o.toString().compareTo(this.nazwa)==0)
				return NAZWA;
			if (o.toString().compareTo(this.tryb)==0)
				return TRYB;
		}
		return -1;
	}
}
