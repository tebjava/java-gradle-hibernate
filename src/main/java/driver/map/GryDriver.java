package driver.map;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Hibernate.Gradle.Gra;

public class GryDriver {
	private List<Gra> gry;
	
	private String poleczenieAdres="jdbc:mysql://192.168.1.220:3306/esport?user=test&password=test";
	
	private Connection polaczenie = null;
	
	public GryDriver() {
		this(new ArrayList<Gra>());
	}
	
	public GryDriver(Gra gra) {
		this.gry = new ArrayList<Gra>();
		this.gry.add(gra);
	}
	
	public GryDriver(List<Gra> gry) {
		this.gry = gry;
	}
	
	public boolean otworzPolaczenie() {
		return otworzPolaczenie(false);
	}
	
	public boolean otworzPolaczenie(boolean test) {
		try {
			polaczenie = DriverManager.getConnection(poleczenieAdres);
			return true;
		}
		catch (SQLException ex) {
			if (test)
				System.err.println("Połączenie nie może zostać ustanowione. Kod błędu " + ex.getErrorCode() + ", komunikat: " + ex.getMessage());
		}
		return false;
	}
	
	public boolean zamknijPolaczenie() {
		return zamknijPolaczenie(false);
	}
	
	public boolean zamknijPolaczenie(boolean test) {
		
			try {
				polaczenie.close();
				return true;
			} catch (SQLException e) {
				if (test) 
					System.err.println("Połączenie nie zostało zamknięte ponieważ nigdy nie zostało otwarte.");
			}
			
		return false;
	}
	
	public void zaladuj() {
		zaladuj(false, false);
	}
	
	public void zaladuj(boolean aktualizacja) {
		zaladuj(aktualizacja, false);
	}
	
	public void zaladuj(boolean aktualizacja, boolean test) {
		try {
			Statement polecenie = polaczenie.createStatement();
			ResultSet wynik = polecenie.executeQuery("SELECT * FROM `gry`;");
			while (wynik.next()) {
				Gra gtmp=new Gra(wynik.getString("nazwa"),wynik.getString("gatunek"),wynik.getString("tryb_gry"),wynik.getLong(1));
				if (aktualizacja && gry.indexOf(gtmp)>-1)
					continue;
				gry.add(gtmp);					
			}
		} catch (SQLException ex) {
			if (test)
				System.err.println("Pobieranie danych nie zakończyło się poprawnie. Kod błędu " + ex.getErrorCode() + ", komunikat: " + ex.getMessage());
		}
	}
	
	public List<Gra> pobierzWszystko() {
		return gry;
	}
	
	public List<Gra> pobierz(Object szukany) {
		return pobierz(szukany,Gra.TYP.ID);
	}
	
	public Gra pobierz(int id) {
		return gry.get(id);
	}
	
	public int pobierzIndex(Object szukany) {
		for (Gra gra:gry) {
			if (gra.zawiera(szukany)==Gra.NAZWA) {
				return gry.indexOf(gra);
			}
		}
		return -1;
	}
	
	public List<Gra> pobierz(Object szukany, int typ) {
		return pobierz(szukany, Gra.TYP.typ(typ));
	}
	
	@SuppressWarnings("static-access")
	public List<Gra> pobierz(Object szukany, Gra.TYP typ) {
		List<Gra> g = new ArrayList<Gra>();
		for (Gra gra : gry) {
			if (gra.zawiera(szukany) != -1) {	
				if (typ.get() == gra.zawiera(szukany))
					g.add(gra);
				if (typ.get() == gra.zawiera(szukany))
					g.add(gra);
				if (typ.get() == gra.zawiera(szukany))
					g.add(gra);
				if (typ.get() == gra.zawiera(szukany))
					g.add(gra);
			}
		}
		return g;
	}
	
	public void zamien(Gra gra, int id) {
		gry.get(id).setGra(gra);
	}
	
	public boolean dodaj(String nazwa) {
		return dodaj(nazwa, "","");
	}
	
	public boolean dodaj(String nazwa, String gatunek) {
		return dodaj(nazwa,gatunek,"");
	}
	
	public boolean dodaj(String nazwa, String gatunek, String tryb) {
		if (pobierz(nazwa,Gra.TYP.NAZWA).isEmpty()) {
			gry.add(new Gra(nazwa,gatunek,tryb));
			return true;
		}
		return false;
	}
	
	public boolean zapisz() {
		String vals="";
		for (Gra gra: gry)
			if (gra.getId()==-1)
				vals+=" ('"+gra.getNazwa()+"','"+gra.getGatunek()+"','"+gra.getTryb()+"'),";
		if (!vals.isEmpty()) {
			vals=vals.substring(0, vals.length()-1);
			try {
				polaczenie.createStatement().execute("INSERT INTO `gry` (`nazwa`,`gatunek`,`tryb_gry`) VALUES"+vals+";");
				return true;
			} catch (SQLException ex) {
				System.err.println("Zapis do bazy zakończył się błędem. Kod błędu " + ex.getErrorCode() + ", komunikat: " + ex.getMessage());
			}
		}
		return false;
	}
	
	public boolean aktualizuj(long id) {
		return aktualizuj((int)id);
	}
	
	public boolean aktualizuj(int id) {
		return aktualizujWszystko(id, "");
	}
	
	public boolean aktualizujWszystko() {
		return aktualizujWszystko(-1, "");
	}
	
	public boolean aktualizuj(String name) {
		return aktualizujWszystko(-1, name);
	}
	
	public boolean aktualizujWszystko(int id, String name) {
		for (Gra gra: gry) {
			String q="";
			if (gra.getId()==id || (id == -1 && name=="")) {
				q="UPDATE `gry` SET `nazwa`='"+gra.getNazwa()+"',`gatunek`='"+gra.getGatunek()+"',`tryb`='"+gra.getTryb()+"' WHERE `id_gra`="+gra.getId()+";";
			}
			if (gra.getNazwa()==name && name!="") {
				q="UPDATE `gry` SET `nazwa`='"+gra.getNazwa()+"',`gatunek`='"+gra.getGatunek()+"',`tryb`='"+gra.getTryb()+"' WHERE `nazwa`="+name+";";
			}
			if (q!="")
				try {
					polaczenie.createStatement().execute(q);
					if (id!=-1)
						return true;
				} catch (SQLException ex) {
					System.err.println("Zapis do bazy zakończył się błędem. Kod błędu " + ex.getErrorCode() + ", komunikat: " + ex.getMessage());
					return false;
				}
		}
		return true;	
	}
	
	public boolean wyczysc() {
		return usun(-1,"");
		
	}
	
	public boolean usun() {
		return wyczysc();
	}
	
	public boolean usun(String nazwa) {
		return usun(-1,nazwa);
	}
	
	public boolean usun(long id) {
		return usun(id,"");
	}
	
	public boolean usun(long id, String nazwa) {
		String zapytanie = "DELETE FROM `gry`";
		long idIN=-1;
		String nazwaIN="(";
		for (Gra gra: gry) {
			if (gra.getId()==id)
				idIN=gra.getId();
			if (gra.getNazwa().compareTo(nazwa)==0)
				nazwaIN+="'"+nazwa+"',";
		}
		if (nazwaIN.length()>1)
			nazwaIN=nazwaIN.substring(0, nazwaIN.length()-1)+")";
		else
			nazwaIN="";
		if (nazwaIN.length()>1||idIN!=-1)
			zapytanie+=" WHERE ";
		if (idIN!=-1)
			zapytanie+="`id_gra`="+idIN;
		if (nazwaIN.length()>1&&idIN!=-1)
			zapytanie+=" AND ";
		else if (zapytanie.contains("id_gra"))
			zapytanie+=";";
		if (nazwaIN.length()>1)
			zapytanie+="`nazwa` IN "+nazwaIN+";";
		if (zapytanie.indexOf(";")==-1 && id==-1 && nazwa.isEmpty())
			zapytanie+=";";
		if (!zapytanie.contains(";")) 
			return false;
		try {
			polaczenie.createStatement().execute(zapytanie);
			System.out.println(zapytanie);
			return true;
		} catch (SQLException ex) {
			System.err.println("Usuwanie z bazy zakończyło się błędem. Kod błędu " + ex.getErrorCode() + ", komunikat: " + ex.getMessage());
			System.out.println(zapytanie);
		}
		return false;	
	}
}
