package driver.map;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import Hibernate.Gradle.Role;

import org.hibernate.Session;

public class RoleMap {
	
	private Session session;
	
	public RoleMap(Session session) {
		this.session=session;
	}
	
	public void save(String nazwa) {
		session.beginTransaction();
		Role rola = new Role();
		rola.setNazwa(nazwa);
		session.save(rola);
		session.getTransaction().commit();
	}
	
	public Role read(String nazwa) {
        @SuppressWarnings("unchecked")
		List<Object> role = session.createSQLQuery("SELECT id_rola FROM role WHERE nazwa='"+nazwa+"'").list();
        if (role.isEmpty())
        	return new Role();
        Role rola = new Role();
        BigInteger bi = (BigInteger) role.get(0);
        rola.setId(bi.longValue());
        rola.setNazwa(nazwa);
		return rola;		
	}
	
	public Role read(long id) {
		Role rola = session.get(Role.class, id);
		return rola;
	}
	
	public List<Role> getAll() {
    	CriteriaBuilder builder = session.getCriteriaBuilder();
    	CriteriaQuery<Role> criteria = builder.createQuery(Role.class);
        criteria.from(Role.class);
        List<Role> role = session.createQuery(criteria).getResultList();
       
		return role;
	}
	
	public boolean update(String staraNazwa, String nowaNazwa) {
		return update(staraNazwa, nowaNazwa, -1);
	}
	
	boolean update(String staraNazwa, String nowaNazwa, long id) {
		Role rola = (id!=-1) ? read(id): read(staraNazwa);
		if (rola == null)
			return false;
		System.out.println(rola.getNazwa());
		rola.setNazwa(nowaNazwa);
		session.beginTransaction();
		session.update(rola);
		session.getTransaction().commit();
		return true;
		//return false;
	}
	
	public boolean delete(String nazwa) {
		return delete(nazwa,-1);
	}
	
	public boolean delete(long id) {
		return delete("",id);
	}
	
	boolean delete(String nazwa, long id) {
		Role rola = (id!=-1) ? read(id): read(nazwa);
		if (rola == null)
			return false;
		session.beginTransaction();
		session.delete(rola);
		session.getTransaction().commit();
		return true;
	}
}
